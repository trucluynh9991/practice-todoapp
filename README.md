# Enjoyword todo-list fe


## How to Setup

- STEP 1: git clone this reponsitory: git@gitlab.com:trucluynh9991/practice-todoapp.git
- STEP 2: cd to the cloned repository: cd firbase-app
- STEP 3: Install the Application with npm

## How to Run App

1. cd to the repo
2. Run & Build
    - Run
        {+ Run npm start +}
        {+ View app on http://localhost:3000/ +}
    - Build
        {+ Run npm build +}
        {+ View build folder in the repository +}

## Frontend Technical Stack

This is the list of important technologies that would be used to implement the project.

## Technical stack
### Languages

**HTML5**
- Hypertext Markup Language revision 5 (HTML5) is a markup language for the structure and presentation of World Wide Web contents. HTML5 supports the traditional HTML and XHTML-style syntax and other new features in its markup, New APIs, XHTML and error handling.

**CSS3**
- A cascading style sheet (CSS) is a Web page derived from multiple sources with a defined order of precedence where the definitions of any style element conflict.

**SASS**
- Sass (Syntactically Awesome Stylesheets) is a style sheet language.
- [Homepage: http://sass-lang.com ] 


**JavaScript**
- An object-oriented computer programming language commonly used to create interactive effects within web browsers.
- ECMAScript 2015 is an ECMAScript standard that was ratified in June 2015. ES2015 is a significant update to the language, and the first major update to the language since ES5 was standardized in 2009. 

### Tools & Libraries

**React**
-  A JavaScript library for building user interface.
- [ Homepage: https://facebook.github.io/react ] 

**Sass**
- CSS loader for Webpack
- [ Homepage: https://github.com/jtangelder/sass-loader] 

**CSS**
- SASS loader for Webpack
- [ Homepage: https://github.com/webpack-contrib/css-loader] 

**Style**
- Style loader for Webpack
- [ Homepage: https://github.com/webpack-contrib/style-loader] 

**Flow**
- Is a Static Type Checker for JavaScript.
- [ Homepage: https://flow.org] 

**React-router**
- Declarative routing for React.
- [ Homepage: https://github.com/ReactTraining/react-router] 

**Redux**
- Redux is a predictable state container for JavaScript apps.
- [ Homepage: https://redux.js.org] 

**Redux-thunk**
- Thunk middleware for Redux.
- [ Homepage: https://github.com/reduxjs/redux-thunk] 

**Redux-saga**
- Is a library that aims to make application side effects
- [ Homepage: https://redux-saga.js.org] 

**Bootstrap**
- Learn how to include React Bootstrap in your project.
- [ Homepage: https://react-bootstrap.github.io] 

**Axios**
- Is used to assist in building API applications.
- [ Homepage: https://github.com/axios/axios] 


**Estimate project**

| Title | Timeline |
| ------ | ------ |
| analysis project | 5h |
| build codebase | 5h |
| code login, register, account page UI | 8h |
| code todo-list page UI | 8h |
| Code login, register, logout features | 8h |
| Code update account features | 4h |
| Code show account features | 2h |
| Code update, edit, delete todo-list features | 4h |
| Code edit todo-list features | 4h |
| Code delete todo-list features | 4h |
| Code fix-bug | 12h |

