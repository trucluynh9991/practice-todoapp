import React from 'react';
import {fire} from './fire';
import {noteData} from './fire';

function App() {
  console.log(fire);
  noteData.once('value').then(function(snapshot){
    console.log(snapshot.val())
  })
  noteData.set({ name: "Ada", age: 36 })
  .then(function() {
   return noteData.once("value");
  })
  .then(function(snapshot) {
    console.log(snapshot.val());
  });
  return (
    <div className="App">
      <h2 className="text-primary">Hello</h2>
    </div>
  );
}

export default App;
