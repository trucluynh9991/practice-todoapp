import {useState,useEffect } from 'react';
import {fire} from '../fire'; 

const Home = () => {
    const [blogs, setBlogs] = useState([]);
    useEffect(() => {
        fire.firestore()
          .collection('blog')
          .onSnapshot(snap => {
            const blogs = snap.docs.map(doc => ({
              id: doc.id,
              ...doc.data()
            }));
            setBlogs(blogs);
          });
      }, []);
    console.log(blogs)
      return (<div></div>);
    }
export default Home;